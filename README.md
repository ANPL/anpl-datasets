ANPL Datasets

Camera calibration:

fx = 554.25;
fy = 554.25;
cu = 320.5;
cv = 240.5;

K = [fx 0 cu;
     0 fy cv;
	 0 0 1];
